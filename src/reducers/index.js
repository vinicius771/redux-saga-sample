import { combineReducers } from 'redux';
import imageReducer from './imageReducer';
import pageReducer from './pageReducer';
import statsReducer from './statsReducer';

const rootReducer = combineReducers({
	nextPage: pageReducer,
	imageStats: statsReducer,
	imageReducer,
});

export default rootReducer;
