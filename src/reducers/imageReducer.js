import { IMAGES } from '../constants';

const initialState = {
	error: null,
	images: [],
	isLoading: false,
};

const imagesReducer = (state = initialState, action) => {
	switch (action.type) {
		case IMAGES.LOAD_FAIL:
			return {
				images: [],
				error: action.error,
				isLoading: false,
			};
		case IMAGES.LOAD_SUCCESS:
			return {
				images: [...state.images, ...action.images],
				error: null,
				isLoading: false,
			};
		case IMAGES.LOAD:
			return {
				...state,
				isLoading: true,
				error: null,
			};
		default:
			return state;
	}
};

export default imagesReducer;
