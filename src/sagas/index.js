import { all } from 'redux-saga/effects';

import imagesSaga from './imagesSaga';
import statsSaga from './statsSaga';

export default function* rootSaga() {
	// The all effect allows multiple sagas to run in parallel
	yield all([imagesSaga(), statsSaga()]);
}
