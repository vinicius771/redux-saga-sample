import { take, call, fork, put } from 'redux-saga/effects';

import { IMAGES } from '../constants';
import { fetchImageStats } from '../api';
import { loadImageStats, setImageStats, setImageStatsError } from '../actions';

export function* handleStatsRequest(id) {
	// Executes three times and dispatches an error in case
	// of three non successfull fetches
	for (let i = 0; i < 3; i++) {
		try {
			// dispacthes the stats containing the image id
			// as a payload
			yield put(loadImageStats(id));

			// blocking call - waits for a promise and
			// suspends the generator when waiting
			const res = yield call(fetchImageStats, id);

			// dispacthes the action for a given id containing
			// as a payoload the total downloads of a particular
			// images
			yield put(setImageStats(id, res.downloads.total));

			// if image stats was loaded exits the generator
			return true;
		} catch (e) {
			// we just need to retry and dispactch an error
			// if we tried more than 3 times
		}
	}

	/** We wanted to retry the api call at least 3 times
	 * before failing. So if the call succeeded the generator
	 * would stop by returning ´true´ (in the try block).
	 *
	 * If the api call failed 3 times, the error handler outside
	 * the loop will be invoked. There isn't any error handler in
	 * the catch block because we still want to retry 3 times
	 * before throwing an error to the handler. */
	yield put(setImageStatsError(id));
}

export default function* watchStatsRequest() {
	/** May alternatively use takeEvery or takeLatest and
	 * invoke the handler saga for the same redux action
	 * that ´take´ is listening on.
	 *
	 * https://redux-saga.js.org/docs/advanced/FutureActions.html*/
	while (true) {
		// A blocking call
		// Gets the images returned by the action when
		// IMAGES.LOAD_SUCCESS is dispatched
		const { images } = yield take(IMAGES.LOAD_SUCCESS);

		/** REMINDER: Saga effects only are allowed inside of for ... loop
		 * NOT in forEACH */
		for (let i = 0; i < images.length; i++) {
			/** fork is a non-blocking middleware which doesn't suspend
			 *  the Generator while waiting for the result of fn.
			 *  Instead as soon as fn is invoked, the Generator resumes
			 *  immediately. */
			yield fork(handleStatsRequest, images[i].id);
		}
	}
}

/** More about ´put´ and ´fork´ in the above for ... loops
 *
 * The first for loop iterate on `fork()` because it is non-blocking
 * and then the second for loop wait for the fetching to
 * complete because `put()` is blocking so this way we can load stats
 * for each image in parallel :)
 */
