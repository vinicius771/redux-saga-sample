import { put, call, takeEvery, select } from 'redux-saga/effects';
import { setImages, setError } from '../actions';
import { IMAGES } from '../constants'; // Action types
import { fetchImages } from '../api'; // The asyncronous API call

// exporting to mock a test on__test__/imagesSaga
export const getPage = (state) => state.nextPage;

/**     W O R K E R   S A G A   */
export function* handleImagesLoad() {
	try {
		// It takes the current state and optionally
		// some arguments and returns a slice of the current Store's state
		const page = yield select(getPage);

		// Makes the API `call` and pass as its params the page value
		// it gets in suspended state until the promise is finished
		const images = yield call(fetchImages, page);

		// put Effect dispatches an action
		// type => IMAGES.LOAD_SUCCESS , payload: images

		// Everytime the IMAGES.LOAD_SUCCESS action is dispatched, the page number increases by one
		yield put(setImages(images));

		// Handles possible errors thrown on fetchImages API call
	} catch (error) {
		// put Effect dispatches an action
		// type => IMAGES.LOAD_FAIL, payload: error
		yield put(setError(error.toString()));
	}
}

/**    W A T C H E R    S A G A   */
export default function* watchImagesLoad() {
	// Non-blocking effect -> runs in parallel
	// invoques the handleImagesLoad worker
	yield takeEvery(IMAGES.LOAD, handleImagesLoad);
}
