# What the Splash!

[Checkout the video series here](https://www.youtube.com/playlist?list=PLMV09mSPNaQlWvqEwF6TfHM-CVM6lXv39)

[![jest](https://jestjs.io/img/jest-badge.svg)](https://github.com/facebook/jest)

An unsplash image gallery built with redux saga

![What the](https://i.imgur.com/nR1iw8P.jpg)

# Starter

After cloning, checkout the `starter` branch

```bash
git checkout starter
```

### Eureka Times While Learning (Vinicius)

#### Generator function\*

A generator is a function that can be paused mid-way at any point during the execution and resumed whenever required. Whenever it needs to generate a value it uses the `yield` keyword **rather than return**. The `yield` keyword halts the function execution and sends a value back to the caller.

#### yield\* x yield call()

`yield*` is useful when you want to **invoke** a `generator function` and **wait** for it to complete.
Alternatively **you can do the same** with a yield `call` **effect**.
The main difference boils down to your preference since both are valid.
`yield call` makes it slightly **easier** in tests to **make assertions** (where you wouldn't want to make assertions on the entire generator).

**Example:**

```javascript
function* anotherGenerator(i) {
	yield i + 1;
	yield i + 2;
	yield i + 3;
}

function* generator(i) {
	yield i;
	yield* anotherGenerator(i);
	yield i + 10;
}

var gen = generator(10);

console.log(gen.next().value); // 10
console.log(gen.next().value); // 11
console.log(gen.next().value); // 12
console.log(gen.next().value); // 13
console.log(gen.next().value); // 20
```

#### Getting action object in saga and passing it to a reducer

Reducers **always receive the action object whenever an action is dispatched**. If you wish to grab the same action object which you're listening for in the **watcher saga** you can simply **add a case for the same action in the reducer**.

Alternatively if you wish to dispatch some action from the saga so that you can listen to it in the reducer, you may use the put effect from redux saga :)

Reducers will listen to any action that is dispatched. **You can dispatch an action from the saga as well using** `put` effect. This is helpful when dealing with things `async`.

For example, say in an app, **both** a reducer and **saga** can listen for a `LOAD_BEGIN` action and later the **saga can dispatch** a `LOAD_END` action when the loading completes. In this case you could use the **reducer** to update a `loading` state in the tree for these two actions.

#### Flow

The `watcher saga` -> listens to dispatched `actions` -> then invoques the `worker saga`.

### Non Blocking and Blocking calls

- **Non-blocking calls:** is a Synchronous flow, which means that the Saga disobey the block of codes and simultaneously read the lines within the parent block.

- **Blocking call** means that the Saga yielded an Effect and await the previous block before it continuous for the next block execution.

```javascript
import {call, cancel, join, take, put} from "redux-saga/effects"

function* saga() {
  // Blocking: will wait for the action
  yield take(ACTION)

  // Blocking: will wait for ApiFn (If ApiFn returns a Promise)
  yield call(ApiFn, ...args)

  // Blocking: will wait for otherSaga to terminate
  yield call(otherSaga, ...args)

  // Non-Blocking: will dispatch within internal scheduler
  yield put(...)

  // Non-blocking: will not wait for otherSaga
  const task = yield fork(otherSaga, ...args)

  // Non-blocking: will resume immediately
  yield cancel(task)


  // Blocking: will wait for the task to terminate
  yield join(task)
}
```
